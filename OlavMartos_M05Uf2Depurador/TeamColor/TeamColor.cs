﻿using System;

namespace TeamColor
{
    class TeamColor
    {
        static void Main()
        {
            string color1 = Console.ReadLine();
            string color2 = Console.ReadLine();
            if (color1 == "white")
            {
                if (color2 == "green") Console.WriteLine("Team1");
                if (color2 == "blue") Console.WriteLine("Team2");
                if (color2 == "brown") Console.Write("Team3");
            } else if (color1 == "red")
            {
                if (color2 == "blue")
                {
                    Console.WriteLine("Team4");
                }
                if (color2 == "black")
                {
                    Console.WriteLine("Team5");
                }
                if (color2 == "green")
                {
                    Console.WriteLine("Team6");
                }
            } else Console.WriteLine("ERROR");
        }

    }
}
