﻿using System;

namespace MATH
{
    internal class MATH
    {
        static void Main()
        {
            var result = 0.0;
            for (int i = 0; i < 10000; i++)
            {
                if (result > 100)
                {
                    result = Math.Sqrt(result);
                }

                if (result < 0)
                {
                    result += result * result;
                }
                result += 20.2;
            }

            Console.WriteLine($"El resultat és {result}");

            /*
             * Quin valor té result quan la i == 1000?
             * 30,762305898749055
             * 
             * El valor result és mai major que 110? I que 120?
             * El valor result es major que 110.
             * Mai es major que 120.
             */
        }

    }
}
